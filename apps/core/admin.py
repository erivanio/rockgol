# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils.text import slugify
from image_cropping import ImageCroppingMixin
from apps.core.forms import UserCreationForm
from apps.core.models import Time, Jogador, Campeonato, Partida, Treino, Usuario, Sorteio, FaseFinal
from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.admin import FlatPageAdmin as FlatPageAdminOld
from django.contrib.flatpages.admin import FlatpageForm as FlatpageFormOld

from django import forms
from ckeditor.widgets import CKEditorWidget


class FlatpageForm(FlatpageFormOld):
    content = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = FlatPage


class FlatPageAdmin(FlatPageAdminOld):
    form = FlatpageForm


class TreinoAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ['nome']
    list_filter = ['status']
    search_fields = ['nome', 'texto']
    date_hierarchy = 'data_criacao'

class JogadorAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ['nome', 'time']
    list_filter = ['status', 'time', 'time__campeonato__nome', 'nome_banda']
    search_fields = ['nome', 'time', 'time__campeonato__nome', 'nome_banda', 'instrumento']
    # date_hierarchy = 'data_criacao'
    ordering = ['time',]

class PartidaAdmin(admin.ModelAdmin):
    list_display = ['time_1', 'time_2', 'grupo', 'campeonato', 'finalizado']
    list_filter = ['status', 'grupo', 'campeonato', 'finalizado']
    search_fields = ['time_1', 'time_2', 'grupo', 'campeonato']
    date_hierarchy = 'data_criacao'


class UsuarioAdmin(admin.ModelAdmin):
    form = UserCreationForm
    add_form = UserCreationForm

    list_display = ('email', 'nome', 'tipo', 'telefone')
    list_filter = ('is_superuser', 'is_active', 'tipo')
    fieldsets = (
        (None, {'fields': ('email', 'password1', 'password2')}),
        ('Informação Pessoal', {'fields': ('tipo', 'nome', 'telefone', 'dt_nasc', 'endereco', 'cpf', 'rg')}),
        ('Permissões', {'fields': ('is_superuser', 'is_active', 'is_staff', 'user_permissions', 'groups')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    search_fields = ('email', 'nome', 'endereco', 'cpf', 'rg')
    ordering = ('email',)

class TimeAdmin(admin.ModelAdmin):

    def save_model(self, request, obj, form, change):
        obj.time_slug = slugify(obj.nome)
        super(TimeAdmin, self).save_model(request, obj, form, change)


class CampeonatoAdmin(ImageCroppingMixin, admin.ModelAdmin):

    def save_model(self, request, obj, form, change):
        obj.campeonato_slug = slugify(obj.nome)
        super(CampeonatoAdmin, self).save_model(request, obj, form, change)

class SorteioAdmin(admin.ModelAdmin):

    list_display = ('nome', 'email', 'telefone', 'get_actions')
    search_fields = ['nome', 'email']

    def get_actions(self, obj):
        try:
            return u'<div align="center"><a class="btn btn-success" id="%s" href="/sorteio/">Realizar Sorteio</a></div>' % (obj.id)
        except:
            return

    get_actions.short_description = 'Ações'
    get_actions.allow_tags = True

admin.site.register(Sorteio, SorteioAdmin)
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageAdmin)
admin.site.register(Usuario, UsuarioAdmin)
admin.site.register(Treino, TreinoAdmin)
admin.site.register(Time, TimeAdmin)
admin.site.register(Jogador, JogadorAdmin)
admin.site.register(Campeonato, CampeonatoAdmin)
admin.site.register(Partida, PartidaAdmin)
admin.site.register(FaseFinal)
