# coding=utf-8
from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register
from dajaxice.utils import deserialize_form
from apps.core.forms import AdicionarJogadorForm


@dajaxice_register
def send_form(request, formulario):
    dajax = Dajax()
    form = AdicionarJogadorForm(deserialize_form(formulario), prefix='jogador')

    if form.is_valid():
        form.save()
        dajax.remove_css_class('#add-jogador input', 'input-error')
        dajax.remove_css_class('#mensagem-feedback', 'mensagem-hidden')
        dajax.add_css_class('#mensagem-feedback', 'success')
        dajax.assign('#mensagem-feedback', 'innerHTML', 'Jogador salvo com sucesso: ')
    else:
        dajax.remove_css_class('#add-jogador input', 'input-error')
        dajax.remove_css_class('#mensagem-feedback', 'mensagem-hidden')
        dajax.add_css_class('#mensagem-feedback', 'alert')
        dajax.assign('#mensagem-feedback', 'innerHTML', 'Corrija os erros abaixo: ')
        for error in form.errors:
            dajax.add_css_class('#id_jogador-%s' % error, 'input-error')

    return dajax.json()