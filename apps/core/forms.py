# -*- coding: utf-8 -*-
from django import forms
from apps.core.models import Usuario, Time, Jogador


class UserCreationForm(forms.ModelForm):
    email = forms.CharField(widget=forms.TextInput(attrs={'type': 'email'}))
    password1 = forms.CharField(label='Senha', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirme a senha', widget=forms.PasswordInput)

    class Meta:
        model = Usuario
        fields = ('email', 'tipo', 'nome', 'telefone', 'endereco', 'cpf', 'rg', 'dt_nasc', 'is_superuser', 'is_active', 'is_staff', 'user_permissions', 'groups')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('As senhas não conferem')
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.is_active = True
        if commit:
            user.save()
        return user

class UsuarioAtualizarSenhaForm(forms.ModelForm):
    class Meta:
        model = Usuario
        exclude = ('email', 'last_login', 'data_criacao', 'password', 'telefone', 'nome', 'endereco',
                   'dt_nasc', 'is_active', 'is_superuser', 'is_staff', 'user_permissions', 'cpf', 'rg', 'groups')

    def clean_password2(self):
        senha_nao_confere = False
        password1 = self.data.get("password1")
        password2 = self.data.get("password2")

        if password1 and password2 and password1 != password2:
            senha_nao_confere = True

        return senha_nao_confere

    def save(self, commit=True):
        user = super(UsuarioAtualizarSenhaForm, self).save(commit=False)
        user.set_password(self.data["password1"])
        user.is_active = True
        if commit:
            user.save()
        return user

class AdicionarTimeForm(forms.ModelForm):
    class Meta:
        model = Time
        exclude = ['slug', 'status', 'data_criacao']

    def __init__(self, *args, **kwargs):
        current_user = kwargs.pop('responsavel')
        super(AdicionarTimeForm, self).__init__(*args, **kwargs)
        self.fields['responsavel'].queryset = Usuario.objects.filter(pk=current_user.id)


class AdicionarJogadorForm(forms.ModelForm):
    class Meta:
        model = Jogador


class AdicionarUsuarioForm(forms.ModelForm):
    class Meta:
        model = Usuario