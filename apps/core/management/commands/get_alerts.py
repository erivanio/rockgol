from datetime import datetime

from django.core.management.base import BaseCommand
from taggit.models import Tag
from haystack.query import SearchQuerySet

from apps.core.models import Usuario, Alerta, Noticia


class Command(BaseCommand):
    def handle(self, *args, **options):

        tags = Tag.objects.all()
        qs = SearchQuerySet().models(Noticia)
        for tag in tags:
            # Busca tag no texto ou no titulo das materias
            noticias = qs.filter(texto=tag.name.lower(), titulo=tag.name.lower())

            for noticia in noticias:
                try:
                    check_tag = unicode(tag.name.lower()) in noticia.titulo.lower() or unicode(tag.name.lower()) in noticia.texto.lower()
                except UnicodeDecodeError:
                    check_tag = False

                if check_tag:
                    usuarios = Usuario.objects.filter(tags__name__in=[tag])
                    for user in usuarios:
                        if not self.verify_alert(user, noticia.object):
                            self.set_alert(user, noticia.object, tag.name)

    def verify_alert(self, user, noticia):
        return Alerta.objects.filter(noticia=noticia, usuario=user).exists()

    def set_alert(self, user, noticia, tag):
        alert = Alerta()
        alert.noticia = noticia
        alert.usuario = user
        alert.tag = tag
        alert.data_envio = datetime.now()
        # if noticia.get('created_at'):
        #     alert.data_noticia = noticia.get('created_at')
        # else:
        #     alert.data_noticia = datetime.today()
        alert.data_insercao = datetime.now()
        return alert.save()
