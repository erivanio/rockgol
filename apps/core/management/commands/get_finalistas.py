from django.core.management import BaseCommand
from django.db.models import Q
from apps.core.models import Tabela

__author__ = 'Wouker'


class Command(BaseCommand):

    def handle(self, *args, **options):
        times_classificados_fase_final = Tabela.objects.filter(Q(time__time_1__campeonato__campeonato_slug=args[0]) | Q(time__time_2__campeonato__campeonato_slug=args[0])).distinct().order_by('-pontos', '-gols_pro', 'gols_contra')
        lista = []
        count = 0
        for times in times_classificados_fase_final:
            count += 1
            if count <= 8:
                lista.append(times.pk)
        times_classificados_fase_final = Tabela.objects.filter(pk__in=lista).order_by('grupo', '-pontos')
        jogos_finais = {}
        cont = 0
        for time in times_classificados_fase_final:
            cont += 1
            if cont == 1 or cont == 4:
                if not jogos_finais.has_key('quartas_1'):
                    jogos_finais['quartas_1'] = []
                    jogos_finais['quartas_1'].append(time.time)
                else:
                    jogos_finais['quartas_1'].append(time.time)
            if cont == 2 or cont == 3:
                if not jogos_finais.has_key('quartas_2'):
                    jogos_finais['quartas_2'] = []
                    jogos_finais['quartas_2'].append(time.time)
                else:
                    jogos_finais['quartas_2'].append(time.time)
            if cont == 5 or cont == 8:
                if not jogos_finais.has_key('quartas_3'):
                    jogos_finais['quartas_3'] = []
                    jogos_finais['quartas_3'].append(time.time)
                else:
                    jogos_finais['quartas_3'].append(time.time)
            if cont == 6 or cont == 7:
                if not jogos_finais.has_key('quartas_4'):
                    jogos_finais['quartas_4'] = []
                    jogos_finais['quartas_4'].append(time.time)
                else:
                    jogos_finais['quartas_4'].append(time.time)

        print jogos_finais


