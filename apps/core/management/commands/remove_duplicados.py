from django.core.management.base import BaseCommand
from apps.core.models import Noticia


class Command(BaseCommand):
    def handle(self, *args, **options):
        noticias = Noticia.objects.raw('select link, array_agg(id) as id from core_noticia group by link having count(*)>1')
        links = list()
        for noticia in noticias:
            if not noticia.link in links:
                links.append(noticia.link)

        for link in links:
            noticias = Noticia.objects.filter(link=link)[1:]
            print link, noticias.count()
            for repetido in noticias:
                repetido.delete()