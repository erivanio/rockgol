# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Time.slug'
        db.delete_column(u'core_time', 'slug')

        # Adding field 'Time.time_slug'
        db.add_column(u'core_time', 'time_slug',
                      self.gf('django.db.models.fields.SlugField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Deleting field 'Campeonato.slug'
        db.delete_column(u'core_campeonato', 'slug')

        # Adding field 'Campeonato.campeonato_slug'
        db.add_column(u'core_campeonato', 'campeonato_slug',
                      self.gf('django.db.models.fields.SlugField')(default='', max_length=100, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Time.slug'
        db.add_column(u'core_time', 'slug',
                      self.gf('django.db.models.fields.SlugField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Deleting field 'Time.time_slug'
        db.delete_column(u'core_time', 'time_slug')

        # Adding field 'Campeonato.slug'
        db.add_column(u'core_campeonato', 'slug',
                      self.gf('django.db.models.fields.SlugField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Deleting field 'Campeonato.campeonato_slug'
        db.delete_column(u'core_campeonato', 'campeonato_slug')


    models = {
        u'core.campeonato': {
            'Meta': {'object_name': 'Campeonato'},
            'campeonato_slug': ('django.db.models.fields.SlugField', [], {'max_length': '100', 'blank': 'True'}),
            'data_criacao': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagem': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'imagem_cortada': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'imagem_listagem': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'core.jogador': {
            'Meta': {'object_name': 'Jogador'},
            'cpf': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '15'}),
            'data_criacao': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data_nascimento': ('django.db.models.fields.DateField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'facebook': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagem': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'imagem_cortada': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'instagram': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'instrumento': ('django.db.models.fields.CharField', [], {'max_length': '70', 'null': 'True', 'blank': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'nome_banda': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'rg': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'telefone': ('django.db.models.fields.CharField', [], {'max_length': '14', 'null': 'True', 'blank': 'True'}),
            'time': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Time']"}),
            'twitter': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'core.partida': {
            'Meta': {'object_name': 'Partida'},
            'campeonato': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Campeonato']"}),
            'data': ('django.db.models.fields.DateField', [], {}),
            'data_criacao': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'finalizado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'grupo': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'placar_time_1': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'placar_time_2': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'time_1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'time_1'", 'to': u"orm['core.Time']"}),
            'time_2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'time_2'", 'to': u"orm['core.Time']"})
        },
        u'core.tabela': {
            'Meta': {'object_name': 'Tabela'},
            'data_criacao': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'derrotas': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'empates': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'genero': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'gols_contra': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'gols_pro': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'grupo': ('django.db.models.fields.CharField', [], {'max_length': '2', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jogos': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'pontos': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'time': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['core.Time']", 'unique': 'True'}),
            'vitorias': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'core.time': {
            'Meta': {'object_name': 'Time'},
            'campeonato': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Campeonato']"}),
            'cor_uniforme': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'data_criacao': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'genero': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'responsavel': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['core.Usuario']"}),
            'sigla': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'time_slug': ('django.db.models.fields.SlugField', [], {'max_length': '100', 'blank': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'core.treino': {
            'Meta': {'object_name': 'Treino'},
            'data_criacao': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagem': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'imagem_cortada': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '100', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'texto': ('django.db.models.fields.TextField', [], {})
        },
        u'core.usuario': {
            'Meta': {'object_name': 'Usuario'},
            'cpf': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'dt_nasc': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'endereco': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'rg': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'telefone': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        }
    }

    complete_apps = ['core']