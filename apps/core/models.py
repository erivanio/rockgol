# -*- coding: utf-8 -*-
import datetime
from django.contrib import auth
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, Permission, \
    _user_get_all_permissions, _user_has_perm, _user_has_module_perms, Group
from django.db import models
from django.db.models.signals import post_save
from django.shortcuts import redirect
from easy_thumbnails.fields import ThumbnailerImageField
from easy_thumbnails.files import get_thumbnailer
from image_cropping import ImageRatioField
from django.db.models import signals, Q
from django.template.defaultfilters import slugify

TIPO_USUARIO = (('1', 'Usuário Sistema'),
                ('2', 'Representante Futebol Masculino'),
                ('3', 'Representante Futebol Feminino'),
                ('4', 'Representante Vôlei'))


class UsuarioManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Usuários devem ter um endereço de email.')

        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password=password, )
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Usuario(AbstractBaseUser):
    email = models.EmailField(verbose_name='Email', max_length=255, unique=True, db_index=True, )
    telefone = models.CharField(max_length=15)
    nome = models.CharField(max_length=200, null=True, blank=True)
    endereco = models.CharField('Endereço', max_length=200, null=True, blank=True)
    dt_nasc = models.DateField('Data de Nascimento', null=True, blank=True)
    cpf = models.CharField(max_length=15)
    rg = models.CharField(max_length=15)
    is_active = models.BooleanField('Ativo?', default=True)
    is_superuser = models.BooleanField('Administrador?', default=False)
    is_staff = models.BooleanField(('Acesso ao Manage?'), default=False)
    user_permissions = models.ManyToManyField(Permission, verbose_name=('Permissões'), blank=True)
    groups = models.ManyToManyField(Group, verbose_name=('groups'), blank=True)
    tipo=models.CharField('Tipo', max_length=1, choices=TIPO_USUARIO, default='1')


    objects = UsuarioManager()

    USERNAME_FIELD = 'email'

    class Meta:
        verbose_name = 'Usuário'
        verbose_name_plural = 'Usuários'

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def __unicode__(self):
        return self.email

    def get_group_permissions(self, obj=None):
        """
        Returns a list of permission strings that this user has through his/her
        groups. This method queries all available auth backends. If an object
        is passed in, only permissions matching this object are returned.
        """
        permissions = set()
        for backend in auth.get_backends():
            if hasattr(backend, "get_group_permissions"):
                if obj is not None:
                    permissions.update(backend.get_group_permissions(self,
                                                                     obj))
                else:
                    permissions.update(backend.get_group_permissions(self))
        return permissions

    def get_all_permissions(self, obj=None):
        return _user_get_all_permissions(self, obj)

    def has_perm(self, perm, obj=None):
        """
        Returns True if the user has the specified permission. This method
        queries all available auth backends, but returns immediately if any
        backend returns True. Thus, a user who has permission from a single
        auth backend is assumed to have permission in general. If an object is
        provided, permissions for this specific object are checked.
        """

        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True

        # Otherwise we need to check the backends.
        return _user_has_perm(self, perm, obj)

    def has_perms(self, perm_list, obj=None):
        """
        Returns True if the user has each of the specified permissions. If
        object is passed, it checks if the user has all required perms for this
        object.
        """
        for perm in perm_list:
            if not self.has_perm(perm, obj):
                return False
        return True

    def has_module_perms(self, app_label):
        """
        Returns True if the user has any permissions in the given app label.
        Uses pretty much the same logic as has_perm, above.
        """
        # Active superusers have all permissions.
        if self.is_active and self.is_superuser:
            return True

        return _user_has_module_perms(self, app_label)


class Treino(models.Model):
    status = models.BooleanField(default=True)
    nome = models.CharField(max_length=150)
    slug = models.SlugField(max_length=100, blank=True)
    texto = models.TextField('Descrição')
    imagem = ThumbnailerImageField(upload_to='uploads/modalidades/%Y/%m')
    imagem_cortada = ImageRatioField('imagem', '270x150')
    data_criacao = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.nome

    def get_imagem_cortada(self):
        return get_thumbnailer(self.imagem).get_thumbnail({
                    'size': (270,150),
                    'box': self.imagem_cortada,
                    'crop': True,
                    'detail': True,
                }).url

    @models.permalink
    def get_absolute_url(self):
        return 'treino-detalhe', [self.slug, self.pk]

    class Meta:
        verbose_name_plural = "Exibições"


def treino_pre_save(signal, instance, sender, **kwargs):
    """Este signal gera um slug automaticamente. Ele verifica se ja existe um
    artigo com o mesmo slug e acrescenta um numero ao final para evitar
    duplicidade"""
    if not instance.slug:
        slug = slugify(instance.nome)
        novo_slug = slug
        contador = 0

        while Treino.objects.filter(slug=novo_slug).exclude(id=instance.id).count() > 0:
            contador += 1
            novo_slug = '%s-%d'%(slug, contador)

        instance.slug = novo_slug
signals.pre_save.connect(treino_pre_save, sender=Treino)

CATEGORIA_CHOICES = (('1', 'Masculino'), ('2', 'Feminino'))
TIPO_CHOICES = (('1', 'Futebol'), ('2', 'Volei'))

class Time(models.Model):
    tipo = models.CharField(max_length=2, choices=TIPO_CHOICES)
    genero = models.CharField(max_length=2, choices=CATEGORIA_CHOICES)
    status = models.BooleanField(default=True)
    nome = models.CharField(max_length=150)
    time_slug = models.SlugField(max_length=100, blank=True)
    sigla = models.CharField(max_length=2)
    campeonato = models.ForeignKey('Campeonato')
    responsavel = models.ForeignKey(Usuario)
    cor_uniforme = models.CharField(max_length=20, null=True, blank=True)
    data_criacao = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.nome

    @models.permalink
    def get_absolute_url(self):
        return 'time_detalhe', [self.campeonato.campeonato_slug, self.time_slug, self.pk]

class Jogador(models.Model):
    status = models.BooleanField(default=True)
    time = models.ForeignKey(Time)
    nome = models.CharField(max_length=150)
    data_nascimento = models.DateField()
    rg = models.CharField(max_length=15)
    cpf = models.CharField(max_length=15, unique=True)
    telefone = models.CharField(max_length=20, blank=True, null=True)
    email = models.EmailField()
    imagem = ThumbnailerImageField(upload_to='uploads/jogador/%Y/%m')
    imagem_cortada = ImageRatioField('imagem', '160x160')
    twitter = models.URLField(blank=True, null=True)
    facebook = models.URLField(blank=True, null=True)
    instagram = models.URLField(blank=True, null=True)
    instrumento = models.CharField(max_length=70, blank=True, null=True)
    nome_banda = models.CharField("Nome da Banda",max_length=150, blank=True, null=True)
    data_criacao = models.DateTimeField(auto_now_add=True)

    def get_imagem_cortada(self):
        return get_thumbnailer(self.imagem).get_thumbnail({
                    'size': (160,160),
                    'box': self.imagem_cortada,
                    'crop': True,
                    'detail': True,
                }).url

    def get_idade(self):
        from datetime import date
        return (date.today() - self.data_nascimento).days / 365

    class Meta:
        verbose_name_plural = 'Jogadores'

    def __unicode__(self):
            return self.nome


class Campeonato(models.Model):
    status = models.BooleanField(default=True)
    imagem_home = ThumbnailerImageField(upload_to='uploads/campeonato/%Y/%m', blank=True, null=True)
    imagem_interna = ThumbnailerImageField(upload_to='uploads/campeonato/%Y/%m', blank=True, null=True)
    imagem_home_cortada = ImageRatioField('imagem_home', '270x150')
    imagem_interna_cortada = ImageRatioField('imagem_interna', '425x172')
    nome = models.CharField("Nome do Campeonato", max_length=100)
    campeonato_slug = models.SlugField(max_length=100, blank=True)
    data_criacao = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.nome

    def get_imagem_cortada(self):
        return get_thumbnailer(self.imagem_home).get_thumbnail({
                    'size': (270,150),
                    'box': self.imagem_home_cortada,
                    'crop': True,
                    'detail': True,
                }).url

    def get_imagem_listagem(self):
        return get_thumbnailer(self.imagem_interna).get_thumbnail({
                    'size': (425,172),
                    'box': self.imagem_interna_cortada,
                    'crop': True,
                    'detail': True,
                }).url

    @models.permalink
    def get_absolute_url(self):
        return 'torneio_listagem', [self.campeonato_slug]

GRUPO_CHOICES = (('1', 'Grupo A'), ('2', 'Grupo B'), ('3', 'Grupo C'), ('4', 'Grupo D'))

class Partida(models.Model):
    CAMPO_CHOICES = (('1', 'Campo I'), ('2', 'Campo II'))

    status = models.BooleanField(default=True)
    finalizado = models.BooleanField(default=False)
    campeonato = models.ForeignKey(Campeonato)
    grupo = models.CharField(max_length=2, choices=GRUPO_CHOICES)
    data = models.DateTimeField()
    local = models.CharField(max_length=2, choices=CAMPO_CHOICES)
    time_1 = models.ForeignKey(Time, related_name='time_1')
    time_2 = models.ForeignKey(Time, related_name='time_2')
    placar_time_1 = models.IntegerField(default=0)
    placar_time_2 = models.IntegerField(default=0)
    data_criacao = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s %i X %i %s' % (self.time_1, self.placar_time_1, self.placar_time_2, self.time_2)


class FaseFinal(models.Model):
    CAMPO_CHOICES = (('1', 'Campo I'), ('2', 'Campo II'))
    FASE_CHOICES = (('1', 'Quartas'), ('2', 'Semi'), ('3', 'Final'), ('4', 'Terceiro Lugar'))

    status = models.BooleanField(default=True)
    finalizado = models.BooleanField(default=False)
    campeonato = models.ForeignKey(Campeonato)
    fase = models.CharField(max_length=2, choices=FASE_CHOICES)
    data = models.DateTimeField()
    local = models.CharField(max_length=2, choices=CAMPO_CHOICES)
    time_1 = models.ForeignKey(Time, related_name='time_1_fase_final')
    time_2 = models.ForeignKey(Time, related_name='time_2_fase_final')
    placar_time_1 = models.IntegerField(default=0)
    placar_time_2 = models.IntegerField(default=0)
    penalti = models.BooleanField(default=False)
    time_1_penalti = models.IntegerField(default=0)
    time_2_penalti = models.IntegerField(default=0)
    data_criacao = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return '%s: %s %i X %i %s' % (self.campeonato.nome, self.time_1, self.placar_time_1, self.placar_time_2, self.time_2)

    class Meta:
        verbose_name = 'Fase final'
        verbose_name_plural = 'Fase final'


class Tabela(models.Model):
    time = models.OneToOneField(Time)
    genero = models.CharField(max_length=2, choices=CATEGORIA_CHOICES)
    grupo = models.CharField(max_length=2, choices=GRUPO_CHOICES, blank=True, null=True)
    jogos = models.IntegerField(default=0)
    vitorias = models.IntegerField(default=0)
    empates = models.IntegerField(default=0)
    derrotas = models.IntegerField(default=0)
    gols_pro = models.IntegerField(default=0)
    gols_contra = models.IntegerField(default=0)
    pontos = models.IntegerField(default=0)
    data_criacao = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.time


def atualiza_tabela(sender, instance, created, **kwargs):
    if not instance.finalizado and created:
        #Atualizar grupo time 1
        time_1_tabela = Tabela.objects.get(time=instance.time_1)
        time_1_tabela.grupo = instance.grupo
        time_1_tabela.genero = instance.time_1.genero
        time_1_tabela.save()

        #Atualizar grupo time 2
        time_2_tabela = Tabela.objects.get(time=instance.time_2)
        time_2_tabela.grupo = instance.grupo
        time_2_tabela.genero = instance.time_2.genero
        time_2_tabela.save()

    if instance.finalizado and instance.status:
        jogo_tabela_1 = Tabela.objects.get(time=instance.time_1)
        jogo_tabela_1.jogos = 0
        jogo_tabela_1.gols_pro = 0
        jogo_tabela_1.gols_contra = 0
        jogo_tabela_1.pontos = 0
        jogo_tabela_1.vitorias = 0
        jogo_tabela_1.derrotas = 0
        jogo_tabela_1.empates = 0
        jogo_tabela_1.save()

        jogo_tabela_2 = Tabela.objects.get(time=instance.time_2)
        jogo_tabela_2.jogos = 0
        jogo_tabela_2.gols_pro = 0
        jogo_tabela_2.gols_contra = 0
        jogo_tabela_2.pontos = 0
        jogo_tabela_2.vitorias = 0
        jogo_tabela_2.derrotas = 0
        jogo_tabela_2.empates = 0
        jogo_tabela_2.save()

        todas_partidas_time_1 = Partida.objects.filter(Q(time_1=instance.time_1, finalizado=True) | Q(time_2 = instance.time_1, finalizado=True))

        for partida_time in todas_partidas_time_1:
            #Atualizar time 1
            time_1_tabela = jogo_tabela_1
            if time_1_tabela.time == partida_time.time_1:
                time_1_tabela.jogos += 1
                time_1_tabela.gols_pro += partida_time.placar_time_1
                time_1_tabela.gols_contra += partida_time.placar_time_2
                time_1_tabela.genero = partida_time.time_1.genero
                time_1_tabela.grupo = partida_time.grupo

                if partida_time.placar_time_1 > partida_time.placar_time_2:
                    time_1_tabela.vitorias += 1
                    time_1_tabela.pontos += 3

                elif partida_time.placar_time_2 > partida_time.placar_time_1:
                    time_1_tabela.derrotas += 1

                else:
                    time_1_tabela.empates += 1
                    time_1_tabela.pontos += 1

                time_1_tabela.save()
            else:
                time_1_tabela.jogos += 1
                time_1_tabela.gols_pro += partida_time.placar_time_2
                time_1_tabela.gols_contra += partida_time.placar_time_1
                time_1_tabela.genero = partida_time.time_2.genero
                time_1_tabela.grupo = partida_time.grupo

                if partida_time.placar_time_2 > partida_time.placar_time_1:
                    time_1_tabela.vitorias += 1
                    time_1_tabela.pontos += 3

                elif partida_time.placar_time_1 > partida_time.placar_time_2:
                    time_1_tabela.derrotas += 1

                else:
                    time_1_tabela.empates += 1
                    time_1_tabela.pontos += 1

                time_1_tabela.save()
        todas_partidas_time_2 = Partida.objects.filter(Q(time_1=instance.time_2, finalizado=True) | Q(time_2=instance.time_2, finalizado=True))

        for partida_time in todas_partidas_time_2:
            #Atualizar time 2
            time_2_tabela = jogo_tabela_2
            if time_2_tabela.time == partida_time.time_2:
                time_2_tabela.jogos += 1
                time_2_tabela.gols_pro += partida_time.placar_time_2
                time_2_tabela.gols_contra += partida_time.placar_time_1
                time_2_tabela.genero = partida_time.time_1.genero
                time_2_tabela.grupo = partida_time.grupo

                if partida_time.placar_time_2 > partida_time.placar_time_1:
                    time_2_tabela.vitorias += 1
                    time_2_tabela.pontos += 3

                elif partida_time.placar_time_1 > partida_time.placar_time_2:
                    time_2_tabela.derrotas += 1

                else:
                    time_2_tabela.empates += 1
                    time_2_tabela.pontos += 1

                time_2_tabela.save()
            else:
                time_2_tabela.jogos += 1
                time_2_tabela.gols_pro += partida_time.placar_time_1
                time_2_tabela.gols_contra += partida_time.placar_time_2
                time_2_tabela.genero = partida_time.time_2.genero
                time_2_tabela.grupo = partida_time.grupo

                if partida_time.placar_time_1 > partida_time.placar_time_2:
                    time_2_tabela.vitorias += 1
                    time_2_tabela.pontos += 3

                elif partida_time.placar_time_2 > partida_time.placar_time_1:
                    time_2_tabela.derrotas += 1

                else:
                    time_2_tabela.empates += 1
                    time_2_tabela.pontos += 1

                time_2_tabela.save()
post_save.connect(atualiza_tabela, sender=Partida, )


def atualiza_time_tabela(sender, instance, **kwargs):
    if instance.status and not Tabela.objects.filter(time=instance):
        tabela = Tabela()
        tabela.time = instance
        tabela.save()

post_save.connect(atualiza_time_tabela, sender=Time, )



class Sorteio(models.Model):

    nome = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    telefone = models.CharField(max_length=15)
    sorteado = models.BooleanField(default=False)

    def __unicode__(self):
        return self.nome

    class Meta:
        ordering = ('nome',)
