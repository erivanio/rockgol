# Create your views here.
from datetime import datetime
from django.contrib import auth
from django.contrib.auth import authenticate, logout
from django.contrib.auth.decorators import login_required
from django.core.management import call_command
from django.db.models import Q
from django.shortcuts import render_to_response, redirect, render
from django.template import RequestContext
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, ListView, DetailView, CreateView
from apps.core.forms import AdicionarTimeForm, AdicionarJogadorForm, AdicionarUsuarioForm, UsuarioAtualizarSenhaForm

from apps.core.models import Campeonato, Partida, Tabela, Treino, Time, Jogador, Sorteio, FaseFinal


class ExibicaoListView(ListView):
    template_name = 'website/modalidades.html'
    model = Treino

    def get_context_data(self, **kwargs):
        context = super(ExibicaoListView, self).get_context_data(**kwargs)
        context['campeonatos'] = Campeonato.objects.filter(status=True)

        return context


class CampeonatoFinaisListView(ListView):
    template_name = 'website/modalidades-finais.html'
    model = Campeonato


class TreinoDetailView(DetailView):
    model = Treino
    template_name = 'website/treinos.html'


class TorneioDetailView(DetailView):
    template_name = 'website/tabela-jogos.html'
    model = Campeonato

    def get_object(self, queryset=None):
        return self.model.objects.get(status=True, campeonato_slug=self.kwargs['campeonato_slug'])

    def get_context_data(self, **kwargs):
        context = super(TorneioDetailView, self).get_context_data(**kwargs)
        context['jogos_1_domingo'] = Partida.objects.filter(data__year=2014, data__month=8, data__day=3, campeonato__campeonato_slug=self.kwargs['campeonato_slug']).order_by('data')
        context['jogos_2_domingo'] = Partida.objects.filter(data__year=2014, data__month=8, data__day=10, campeonato__campeonato_slug=self.kwargs['campeonato_slug']).order_by('data')
        context['jogos_3_domingo'] = Partida.objects.filter(data__year=2014, data__month=8, data__day=17, campeonato__campeonato_slug=self.kwargs['campeonato_slug']).order_by('data')
        context['jogos_4_domingo'] = Partida.objects.filter(data__year=2014, data__month=8, data__day=24, campeonato__campeonato_slug=self.kwargs['campeonato_slug']).order_by('data')
        context['jogos_5_domingo'] = Partida.objects.filter(data__year=2014, data__month=8, data__day=31, campeonato__campeonato_slug=self.kwargs['campeonato_slug']).order_by('data')
        context['tabelas_jogos_grupo_a'] = Tabela.objects.filter(Q(time__time_1__campeonato__campeonato_slug=self.kwargs['campeonato_slug']) | Q(time__time_2__campeonato__campeonato_slug=self.kwargs['campeonato_slug']), grupo='1').distinct().order_by('-pontos', '-gols_pro', 'gols_contra')
        context['tabelas_jogos_grupo_b'] = Tabela.objects.filter(Q(time__time_1__campeonato__campeonato_slug=self.kwargs['campeonato_slug']) | Q(time__time_2__campeonato__campeonato_slug=self.kwargs['campeonato_slug']), grupo='2').distinct().order_by('-pontos', '-gols_pro', 'gols_contra')
        context['tabelas_jogos_grupo_c'] = Tabela.objects.filter(Q(time__time_1__campeonato__campeonato_slug=self.kwargs['campeonato_slug']) | Q(time__time_2__campeonato__campeonato_slug=self.kwargs['campeonato_slug']), grupo='3').distinct().order_by('-pontos', '-gols_pro', 'gols_contra')
        context['tabelas_jogos_grupo_d'] = Tabela.objects.filter(Q(time__time_1__campeonato__campeonato_slug=self.kwargs['campeonato_slug']) | Q(time__time_2__campeonato__campeonato_slug=self.kwargs['campeonato_slug']), grupo='4').distinct().order_by('-pontos', '-gols_pro', 'gols_contra')

        return context


# class SegundaEtapaDetailView(DetailView):
#     template_name = 'website/pagina-teste.html'
#     model = Campeonato
#
#     def get_object(self, queryset=None):
#         return self.model.objects.get(status=True, campeonato_slug=self.kwargs['campeonato_slug'])
#
#     def get_context_data(self, **kwargs):
#         context = super(SegundaEtapaDetailView, self).get_context_data(**kwargs)
#         times_classificados_fase_final = Tabela.objects.filter(Q(time__time_1__campeonato__campeonato_slug=self.kwargs['campeonato_slug']) | Q(time__time_2__campeonato__campeonato_slug=self.kwargs['campeonato_slug'])).distinct().order_by('-pontos', '-gols_pro', 'gols_contra')
#         x = call_command('get_finalistas', 'campeonato-de-futebol-masculino-2014')
#         lista = []
#         count = 0
#         for times in times_classificados_fase_final:
#             count += 1
#             if count <= 8:
#                 lista.append(times.pk)
#         context['times_classificados_fase_final'] = Tabela.objects.filter(pk__in=lista).order_by('grupo', '-pontos')
#
#         return context


class SegundaEtapaDetailView(DetailView):
    template_name = 'website/finais.html'
    model = Campeonato

    def get_object(self, queryset=None):
        return self.model.objects.get(status=True, campeonato_slug=self.kwargs['campeonato_slug'])

    def get_context_data(self, **kwargs):
        context = super(SegundaEtapaDetailView, self).get_context_data(**kwargs)
        context['quartas'] = FaseFinal.objects.filter(fase='1', campeonato__campeonato_slug=self.kwargs['campeonato_slug'])
        context['semi'] = FaseFinal.objects.filter(fase='2', campeonato__campeonato_slug=self.kwargs['campeonato_slug'])
        context['final'] = FaseFinal.objects.filter(fase='3', campeonato__campeonato_slug=self.kwargs['campeonato_slug'])
        context['terceiro'] = FaseFinal.objects.filter(fase='4', campeonato__campeonato_slug=self.kwargs['campeonato_slug'])

        return context


class TimeDetailView(DetailView):
    model = Time
    template_name = 'website/time.html'

    def get_object(self, queryset=None):
        return self.model.objects.get(status=True, time_slug=self.kwargs['time_slug'])

    def get_context_data(self, **kwargs):
        context = super(TimeDetailView, self).get_context_data(**kwargs)
        context['time'] = Time.objects.get(time_slug=self.kwargs['time_slug'])
        context['time_tabela'] = Tabela.objects.get(time__time_slug=self.kwargs['time_slug'])
        context['todos_times_tabela'] = Tabela.objects.filter(Q(time__time_1__campeonato__campeonato_slug=context['time_tabela'].time.campeonato.campeonato_slug, grupo=context['time_tabela'].grupo) | Q(time__time_2__campeonato__campeonato_slug=context['time_tabela'].time.campeonato.campeonato_slug, grupo=context['time_tabela'].grupo)).distinct().order_by('-pontos', '-gols_pro', 'gols_contra')
        return context


@login_required
def minha_conta(request):
    form_time = AdicionarTimeForm(responsavel=request.user, prefix='time')
    form_jogador = AdicionarJogadorForm(prefix='jogador')
    form_usuario = UsuarioAtualizarSenhaForm()
    try:
        time = Time.objects.filter(responsavel=request.user)[0]
        jogadores = Jogador.objects.filter(time=time)
    except:
        pass

    if request.method == 'POST':
        if request.POST.has_key('time-responsavel'):
            form_time = AdicionarTimeForm(request.POST, responsavel=request.user, prefix='time')
            if form_time.is_valid():
                form_time.save()
            return redirect('minha-conta')

        elif request.POST.has_key('jogador-nome'):
            form_jogador = AdicionarJogadorForm(request.POST, request.FILES, prefix='jogador')
            if form_jogador.is_valid():
                form_jogador.save()
            return redirect('minha-conta')

    return render_to_response('website/minha-conta.html', locals(), context_instance=RequestContext(request))

def login(request):
    user = None
    if request.POST:
        emailUser = request.POST.get('email')
        senhaUser = request.POST.get('password')
        user = authenticate(email=emailUser, password=senhaUser)
    if user is not None:
        if user.is_active:
            auth.login(request, user)
            return redirect('minha-conta')
        else:
            return redirect('home')
    else:
        return redirect('home')


def sair(request):
    logout(request)
    return redirect('home')


def sorteio(request):
    sorteado = Sorteio.objects.filter(sorteado=False).order_by('?')[0]
    sorteado.sorteado = True
    sorteado.save()
    return render(request, 'website/sorteio.html', {'sorteado': sorteado})
