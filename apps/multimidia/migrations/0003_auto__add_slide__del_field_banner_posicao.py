# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Slide'
        db.create_table(u'multimidia_slide', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('imagem', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('imagem_slide', self.gf(u'django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('publicado_em', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('remover_em', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal(u'multimidia', ['Slide'])

        # Deleting field 'Banner.posicao'
        db.delete_column(u'multimidia_banner', 'posicao')


    def backwards(self, orm):
        # Deleting model 'Slide'
        db.delete_table(u'multimidia_slide')

        # Adding field 'Banner.posicao'
        db.add_column(u'multimidia_banner', 'posicao',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=1),
                      keep_default=False)


    models = {
        u'multimidia.album': {
            'Meta': {'ordering': "('-cadastrado_em',)", 'object_name': 'Album'},
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'creditos': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'foto_big': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'foto_destaque': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'foto_small': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'multimidia.banner': {
            'Meta': {'object_name': 'Banner'},
            'arquivo': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'remover_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'multimidia.foto': {
            'Meta': {'ordering': "('-cadastrado_em',)", 'object_name': 'Foto'},
            'album': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'fotos'", 'to': u"orm['multimidia.Album']"}),
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'foto_big': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'foto_small': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'foto_thumb': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legenda': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'multimidia.slide': {
            'Meta': {'object_name': 'Slide'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagem': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'imagem_slide': (u'django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'remover_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'multimidia.video': {
            'Meta': {'ordering': "('-publicado_em',)", 'object_name': 'Video'},
            'cadastrado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'foto': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publicado_em': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '100', 'blank': 'True'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'video': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'video_id': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['multimidia']