# -*- coding: utf-8 -*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from apps.multimidia.models import Album


def album(request, slug, id):
    album = get_object_or_404(Album, slug = slug,id=id, status = True)
    try:
        titulo = album.titulo + ' - '
    except:
        titulo = ''

    return render_to_response('website/galeria-interna.html', locals(),
                              context_instance=RequestContext(request))


# def videos(request):
#     videos = Video.objects.all()
#     paginator = Paginator(videos, 4)
#     try:
#         page = int(request.GET.get('page', '1'))
#     except ValueError:
#         page = 1
#     try:
#         videos = paginator.page(page)
#     except (EmptyPage, InvalidPage):
#         videos = paginator.page(paginator.num_pages)
#     # agenda_passados = Agenda.objects.filter(horario__lte=datetime.now)
#
#     return render_to_response('floriano/multimidia/videos.html', locals(),
#         context_instance=RequestContext(request))