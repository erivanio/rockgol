# -*- coding: utf-8 -*-
from django.contrib import admin
from image_cropping import ImageCroppingMixin
from apps.website.models import Noticia, Apoio, Discotecagem, Atracao, Organizacao
from django.db import models
from ckeditor.widgets import CKEditorWidget

class NoticiaAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('titulo', 'autor')
    search_fields = ['titulo', 'subtitulo']
    date_hierarchy = 'data_criacao'
    formfield_overrides = {models.TextField: {'widget': CKEditorWidget}}

admin.site.register(Noticia, NoticiaAdmin)


class OrganizacaoAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('nome', 'cargo')
    search_fields = ['nome', 'cargo']

admin.site.register(Organizacao, OrganizacaoAdmin)


class ApoioAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('titulo', 'posicao')
    list_filter = ['posicao', 'titulo']
    search_fields = ['titulo', 'posicao']
    date_hierarchy = 'publicado_em'

admin.site.register(Apoio, ApoioAdmin)


class DiscotecagemInline(ImageCroppingMixin, admin.TabularInline):
    model = Discotecagem
    extra = 1


class AtracaoInline(admin.ModelAdmin):
    inlines = [
        DiscotecagemInline,
    ]
admin.site.register(Atracao, AtracaoInline)