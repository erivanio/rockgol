# coding=utf-8
from datetime import datetime
from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from image_cropping import ImageRatioField
from easy_thumbnails.files import get_thumbnailer
from django.db.models import signals
from django.template.defaultfilters import slugify
from apps.core.models import Usuario

PATROCINADOR_CHOICES = (('1', 'Patrocinio'), ('2', 'Apoio'))


class Apoio(models.Model):
    titulo=models.CharField(max_length=100)
    descricao = models.TextField()
    imagem=models.ImageField('Imagem Interna',upload_to='uploads/apoio/%Y/%m/')
    imagem_listagem = ImageRatioField('imagem', '180x180')
    imagem_interna = ImageRatioField('imagem', '420x420')
    imagem_sidebar = models.ImageField(upload_to='uploads/apoio/%Y/%m/')
    imagem_sidebar_crop = ImageRatioField('imagem_sidebar', '227x77')
    slug = models.SlugField(max_length=100, blank=True)
    publicado_em = models.DateTimeField(verbose_name='Data de Publicacao', default=datetime.now)
    posicao=models.CharField('Tipo', max_length=1, choices=PATROCINADOR_CHOICES)
    url=models.URLField("Site", blank=True, null=True)
    status=models.BooleanField(verbose_name='Ativo? ', default=True)

    def get_imagem_listagem(self):
        return get_thumbnailer(self.imagem).get_thumbnail({
                    'size': (180,180),
                    'box': self.imagem_listagem,
                    'crop': True,
                    'detail': True,
                }).url

    def get_imagem_interna(self):
        return get_thumbnailer(self.imagem).get_thumbnail({
                    'size': (420,420),
                    'box': self.imagem_interna,
                    'crop': True,
                    'detail': True,
                }).url

    def get_imagem_sidebar(self):
        return get_thumbnailer(self.imagem_sidebar).get_thumbnail({
                    'size': (227,77),
                    'box': self.imagem_sidebar_crop,
                    'crop': True,
                    'detail': True,
                }).url

    def get_absolute_url(self):
        return ('/apoio/%s-%i.html' % (self.slug, self.id))

    class Meta:
        verbose_name_plural = "Patrocinadores"

    def __unicode__(self):
        return self.titulo

def apoio_pre_save(signal, instance, sender, **kwargs):
    """Este signal gera um slug automaticamente. Ele verifica se ja existe um
    artigo com o mesmo slug e acrescenta um numero ao final para evitar
    duplicidade"""
    if not instance.slug:
        slug = slugify(instance.titulo)
        novo_slug = slug
        contador = 0

        while Apoio.objects.filter(slug=novo_slug).exclude(id=instance.id).count() > 0:
            contador += 1
            novo_slug = '%s-%d'%(slug, contador)

        instance.slug = novo_slug
signals.pre_save.connect(apoio_pre_save, sender=Apoio)

class Noticia(models.Model):
    titulo = models.CharField(max_length=75)
    subtitulo = models.CharField(max_length=75)
    texto = models.TextField()
    imagem = ThumbnailerImageField(upload_to='uploads/noticias/%Y/%m', blank=True, null=True)
    imagem_listagem = ImageRatioField('imagem', '430x250')
    imagem_interna = ImageRatioField('imagem', '250x250')
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=150, blank=True)
    autor = models.ForeignKey(Usuario, blank=True)
    data_criacao = models.DateTimeField(auto_now_add=True)
    data_publicacao = models.DateTimeField(default=datetime.now())
    fonte = models.CharField(max_length = 200, null = True, blank = True)

    def get_imagem_listagem(self):
        return get_thumbnailer(self.imagem).get_thumbnail({
                    'size': (430,250),
                    'box': self.imagem_listagem,
                    'crop': True,
                    'detail': True,
                }).url

    def get_imagem_interna(self):
        return get_thumbnailer(self.imagem).get_thumbnail({
                    'size': (250,250),
                    'box': self.imagem_interna,
                    'crop': True,
                    'detail': True,
                }).url

    @models.permalink
    def get_absolute_url(self):
        return 'noticia-detalhe', [self.slug, self.pk]

    def __unicode__(self):
        return self.titulo

    class Meta:
        ordering = ('data_criacao',)


def noticia_pre_save(signal, instance, sender, **kwargs):
    """Este signal gera um slug automaticamente. Ele verifica se ja existe um
    artigo com o mesmo slug e acrescenta um numero ao final para evitar
    duplicidade"""
    if not instance.slug:
        slug = slugify(instance.titulo)
        novo_slug = slug
        contador = 0

        while Noticia.objects.filter(slug=novo_slug).exclude(id=instance.id).count() > 0:
            contador += 1
            novo_slug = '%s-%d'%(slug, contador)

        instance.slug = novo_slug
signals.pre_save.connect(noticia_pre_save, sender=Noticia)


class Organizacao(models.Model):
    nome = models.CharField(max_length=75)
    cargo = models.CharField(max_length=75)
    imagem = ThumbnailerImageField(upload_to='uploads/organizacao/%Y/%m', blank=True, null=True)
    imagem_cortada = ImageRatioField('imagem', '150x150')
    twitter = models.URLField(blank=True, null=True)
    facebook = models.URLField(blank=True, null=True)
    instagram = models.URLField(blank=True, null=True)

    def get_imagem_cortada(self):
        return get_thumbnailer(self.imagem).get_thumbnail({
                    'size': (150,150),
                    'box': self.imagem_cortada,
                    'crop': True,
                    'detail': True,
                }).url

    def __unicode__(self):
        return self.nome
    class Meta:
        verbose_name_plural = "Organizadores"

class Atracao(models.Model):
    status = models.BooleanField(default=True)
    nome = models.CharField(max_length=150)
    data = models.DateField()
    data_criacao = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.nome
    class Meta:
        verbose_name_plural = "Atrações"


class Discotecagem(models.Model):
    status = models.BooleanField(default=True)
    nome = models.CharField(max_length=150)
    foto = ThumbnailerImageField(upload_to='uploads/discotecagem/%Y/%m', blank=True, null=True)
    foto_cortada = ImageRatioField('imagem', '430x175')
    data_criacao = models.DateTimeField(auto_now_add=True)
    discotecagem = models.ForeignKey(Atracao)

    def get_imagem_cortada(self):
        return get_thumbnailer(self.foto).get_thumbnail({
                    'size': (430,175),
                    'box': self.foto_cortada,
                    'crop': True,
                    'detail': True,
                }).url

    def __unicode__(self):
            return self.nome
    class Meta:
        verbose_name_plural = "Discotecagens"

