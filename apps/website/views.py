# -*- coding: utf-8 -*-
from datetime import datetime

from django.shortcuts import redirect, render_to_response
from django.template import RequestContext
from django.views.generic import TemplateView, ListView, DetailView
from apps.core.models import Campeonato, Treino, Tabela
from apps.multimidia.models import Banner, Album
from apps.website.forms import FormContato
from apps.website.models import Noticia, Apoio, Atracao, Organizacao


class HomePageView(TemplateView):
    template_name = 'website/index.html'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['campeonato'] = Campeonato.objects.filter(status=True).order_by('nome')
        context['noticias'] = Noticia.objects.filter(status=True)[:3]
        context['treinos'] = Treino.objects.filter(status=True).order_by('?')[:4]
        context['tabelas_jogos_masculino_grupo_a'] = Tabela.objects.filter(grupo='1', genero='1').distinct().order_by('pontos')
        context['tabelas_jogos_masculino_grupo_b'] = Tabela.objects.filter(grupo='2', genero='1').distinct().order_by('pontos')
        context['tabelas_jogos_masculino_grupo_c'] = Tabela.objects.filter(grupo='3', genero='1').distinct().order_by('pontos')
        context['tabelas_jogos_masculino_grupo_d'] = Tabela.objects.filter(grupo='4', genero='1').distinct().order_by('pontos')

        context['tabelas_jogos_feminino_grupo_a'] = Tabela.objects.filter(grupo='1', genero='2').distinct().order_by('pontos')
        context['tabelas_jogos_feminino_grupo_b'] = Tabela.objects.filter(grupo='2', genero='2').distinct().order_by('pontos')
        context['tabelas_jogos_feminino_grupo_c'] = Tabela.objects.filter(grupo='3', genero='2').distinct().order_by('pontos')
        context['tabelas_jogos_feminino_grupo_d'] = Tabela.objects.filter(grupo='4', genero='2').distinct().order_by('pontos')

        try:
            context['banner_medio'] = Banner.objects.filter(status=True, publicado_em__lte=datetime.now(), remover_em__gte=datetime.now())[0]
            context['album'] = Album.objects.filter(status=True)[0]
        except:
            return context
        return context


class NoticiasView(ListView):
    template_name = 'website/noticias.html'
    model = Noticia
    paginate_by = 8

    def get_queryset(self):
        return self.model.objects.filter(status=True).order_by('-data_criacao')

class GaleriaListView(ListView):
    template_name = 'website/listagem-galeria.html'
    model = Album

    def get_queryset(self):
        return self.model.objects.filter(status=True).order_by('-cadastrado_em')

class NoticiaDetailView(DetailView):
    model = Noticia
    template_name = 'website/noticia.html'

class AtracoesView(ListView):
    template_name = 'website/atracoes.html'
    model = Atracao

    def get_queryset(self):
        return self.model.objects.filter(status=True).order_by('-data_criacao')


class OrganizacaoView(ListView):
    template_name = 'website/organizacao.html'
    model = Organizacao


class ParceirosView(ListView):
    template_name = 'website/parceiros.html'
    model = Apoio

    def get_queryset(self):
        return self.model.objects.filter(status=True).order_by('posicao')


class ApoioView(DetailView):
    model = Apoio
    template_name = 'website/parceiros-interna.html'

def contato(request):
    if request.method == 'POST':
        form = FormContato(request.POST)
        if form.is_valid():
            form.enviar()
            mostrar = 'Mensagem enviada!'
            form = FormContato()
            return redirect('/')
    else:
        form = FormContato()

    return render_to_response('website/contatos.html', locals(), context_instance=RequestContext(request))
