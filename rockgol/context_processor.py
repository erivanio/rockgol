from datetime import datetime
from apps.multimidia.models import Slide
from apps.website.models import Apoio


def estaticos(request):
    try:
        slides = Slide.objects.filter(status=True, publicado_em__lte=datetime.now(), remover_em__gte=datetime.now())
        patrocinadores = Apoio.objects.filter(posicao='1', status=True)[:5]
        apoios = Apoio.objects.filter(posicao='2', status=True)
        return {'slides': slides, 'patrocinadores':patrocinadores, 'apoios':apoios}
    except:
        return {}

