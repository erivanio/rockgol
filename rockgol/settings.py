# coding=utf-8
import os
from decouple import config

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

TEMPLATE_DIRS = (os.path.join(BASE_DIR, 'templates'),)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = (os.path.join(MEDIA_ROOT, 'static'),)

DEBUG = config('DEBUG', cast=bool)

TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Nataliel Vasconcelos', 'nataliel.vasconcelos@gmail.com'),
    ('Erivanio Vasconcelos', 'erivanio.vasconcelos@gmail.com'),
    ('Gabriel Freitas', 'gabrielfreitas07@gmail.com'),
    ('Gustavo Carvalho', 'gt.salles@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': config('DB_NAME'),
        'USER': config('DB_USER'),
        'PASSWORD': config('DB_PWD', default=''),
        'HOST': '',
        'PORT': '',
    }
}

ALLOWED_HOSTS = ['*']

TIME_ZONE = 'America/Fortaleza'

LANGUAGE_CODE = 'pt-BR'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = False

AUTH_USER_MODEL = 'core.Usuario'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'dajaxice.finders.DajaxiceFinder',
)

SECRET_KEY = '*uez(ve8plw!f%a!+5*6-qt5av1#tw$9a@npiy5aazy=y%wc#o'

from django.conf import global_settings

TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    'rockgol.context_processor.estaticos',
    'django.core.context_processors.request',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

#ROOT_URLCONF = 'pegasys.urls'
#WSGI_APPLICATION = 'pegasys.wsgi.application'
ROOT_URLCONF = '%s.urls' % config('ROOT_URLCONF')
WSGI_APPLICATION = '%s.wsgi.application' % config('WSGI_APPLICATION')

INSTALLED_APPS = (
    'easy_thumbnails',
    'image_cropping',
    'ckeditor',
    'multiupload',
    'south',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',
    # 'dajaxice',
    # 'dajax',
    'suit',
    'django.contrib.admin',
    'widget_tweaks',
    'debug_toolbar',


)

from easy_thumbnails.conf import Settings as thumbnail_settings
THUMBNAIL_PROCESSORS = (
    'image_cropping.thumbnail_processors.crop_corners',
) + thumbnail_settings.THUMBNAIL_PROCESSORS

SOUTH_MIGRATION_MODULES = {
        'easy_thumbnails': 'easy_thumbnails.south_migrations',
    }

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

CKEDITOR_UPLOAD_PATH = os.path.join(MEDIA_ROOT, 'uploads/ckeditor')

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': [
            # {'name': 'document', 'items': ['Source', '-', 'Save', 'NewPage','DocProps','Preview','Print','-','Templates' ] },
            {'name': 'clipboard',  'items': ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ]},
            {'name': 'editing', 'items': ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt']},
            {'name': 'links', 'items': ['Link', 'Unlink']},
            {'name': 'insert', 'items': ['Image', 'Flash', 'Table', 'HorizontalRule', 'SpecialChar', 'PageBreak', 'Iframe']},
            '/',
            {'name': 'basicstyles', 'items': ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
            {'name': 'paragraph', 'items': ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
                                            '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
            '/',
            {'name': 'styles', 'items': ['Styles', 'Format', 'Font', 'FontSize']},
            {'name': 'colors', 'items': ['TextColor', 'BGColor']},
            {'name': 'tools', 'items': ['Maximize', 'ShowBlocks']}
        ],
        'height': 300,
        'width': 615,
    },
}

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'email@gmail.com'
EMAIL_HOST_PASSWORD = 'senha'
EMAIL_SUBJECT_PREFIX = '[RockGol]'
EMAIL_USE_TLS = True
EMAIL_PORT = 587

INSTALLED_APPS += ('apps.website', 'apps.multimidia', 'apps.core',)

SUIT_CONFIG = {
    'ADMIN_NAME': 'Rock Gol',

    'MENU': (
        {'label': u'Configuração', 'icon':'ion ion-ios7-cog', 'models': ('auth.user', 'auth.group', 'sites.site')},
        {'app': 'multimidia', 'label': u'Multimídia', 'icon':'ion ion-ios7-briefcase-outline', 'color':'bg-green'},
        {'app': 'core', 'label': 'Core', 'icon':'ion ion-ios7-briefcase-outline', 'color':'bg-blue'},
        {'app': 'website', 'label': 'Institucional', 'icon':'ion ion-ios7-briefcase-outline', 'color':'bg-red'},
        {'app': 'flatpages', 'label': 'Páginas Planas', 'icon':'ion ion-ios7-briefcase-outline', 'color':'bg-red'},
    )
}