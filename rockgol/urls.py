from dajaxice.core import dajaxice_autodiscover, dajaxice_config
from django.conf.urls import patterns, include, url
from apps.core.views import TorneioDetailView, TreinoDetailView, ExibicaoListView, TimeDetailView, \
    SegundaEtapaDetailView, CampeonatoFinaisListView

from apps.website.views import HomePageView, NoticiasView, ParceirosView, ApoioView, AtracoesView, OrganizacaoView, \
    NoticiaDetailView, GaleriaListView
from rockgol import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
dajaxice_autodiscover()
admin.autodiscover()

urlpatterns = patterns('',
    (r'^media/(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    (r'^static/(.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^manage-cms/', include(admin.site.urls)),
    (r'^ckeditor/', include('ckeditor.urls')),
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^torneios/(?P<campeonato_slug>[\w_-]+)/$', TorneioDetailView.as_view(), name='torneio_listagem'),
    url(r'^torneios/(?P<campeonato_slug>[\w_-]+)/segunda-etapa/$', SegundaEtapaDetailView.as_view(), name='segunda_etapa_listagem'),
    url(r'^torneios/(?P<campeonato_slug>[\w_-]+)/(?P<time_slug>[\w_-]+)-(?P<id>\d+).html', TimeDetailView.as_view(), name='time_detalhe'),
    url(r'^exibicoes/$', ExibicaoListView.as_view(), name='modalidades'),
    url(r'^fase-final/$', CampeonatoFinaisListView.as_view(), name='fase_final_listagem'),
    url(r'^exibicoes/(?P<slug>[\w_-]+)-(?P<id>\d+).html', TreinoDetailView.as_view(), name='treino-detalhe'),
    url(r'^noticias/$', NoticiasView.as_view(), name='noticias'),
    url(r'^noticias/(?P<slug>[\w_-]+)-(?P<id>\d+).html', NoticiaDetailView.as_view(), name='noticia-detalhe'),
    url(r'^galerias/$', GaleriaListView.as_view(), name='galerias'),
    url(r'^atracoes/$', AtracoesView.as_view(), name='atracoes'),
    url(r'^organizacao/$', OrganizacaoView.as_view(), name='organizacao'),
    url(r'^parceiros/$', ParceirosView.as_view(), name='parceiros'),
    url(r'^apoio/(?P<slug>[\w_-]+)-(?P<id>\d+).html', ApoioView.as_view(), name='parceiro-interna'),
    (r'^albuns/(?P<slug>[\w_-]+)-(?P<id>\d+).html', 'apps.multimidia.views.album'),
    url(r'^contato/$', 'apps.website.views.contato', name='contato'),
    url(r'^minha-conta/$', 'apps.core.views.minha_conta', name='minha-conta'),
    url(r'login/$', 'apps.core.views.login', name='login'),
    url(r'^sair/$', 'apps.core.views.sair', name='sair'),
    url(r'sorteio/$', 'apps.core.views.sorteio', name='sorteio'),

)
